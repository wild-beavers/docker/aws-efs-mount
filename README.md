# AWS EFS mount

## Description
This image contains AWS EFS tools. https://github.com/aws/efs-utils
The image is based on the official ubuntu image.
This image force to use EFS with TLS (with `stunnel`).
More details about in [transit encryption](https://docs.aws.amazon.com/efs/latest/ug/encryption-in-transit.html) and [IAM authentication](https://docs.aws.amazon.com/efs/latest/ug/auth-and-access-control.html)

This image supports [sd_notify(3)](https://www.freedesktop.org/software/systemd/man/sd_notify.html).

## Usage
This image:
* Mounts an EFS at `/mnt/efs`;
* needs AWS credentials - must be volume mounted at `/root/.aws/`;
* needs the kernel capacity [`CAP_SYS_ADMIN`](https://man7.org/linux/man-pages/man7/capabilities.7.html);

To be able to interact with your mount point outside the docker container, you must set the [bind-propagation](https://docs.docker.com/storage/bind-mounts/#configure-bind-propagation) to `shared`

For a simple usage in AWS VPC with an EC2 role:
```shell
sudo podman run -it --rm -d \
  --cap-add=CAP_SYS_ADMIN \
  -v "$(pwd)/efs":/mnt/efs:shared \
  --health-cmd '/usr/local/bin/healthcheck' \
  --health-interval 20s \
  --healthcheck-timeout 5s \
  --health-retries 3 \
  --health-start-period 60s \
  --health-timeout 5s \
  registry.gitlab.com/wild-beavers/docker/aws-efs-mount:latest fs-1234567890abcdefg.efs.us-east-1.amazonaws.com
```

For usage outside AWS with endpoint IP:
```shell
sudo podman run -it --rm -d \
  --cap-add=CAP_SYS_ADMIN \
  -e "EFS_MOUNT_ENDPOINT_IP=172.16.0.2" \
  -v "${HOME}/.aws/credentials":/root/.aws/credentials \
  -v "${HOME}/.aws/config":/root/.aws/config \
  -v "$(pwd)/mountpoint":/mnt/efs:shared \
  --health-cmd '/usr/local/bin/healthcheck' \
  --health-interval 20s \
  --healthcheck-timeout 5s \
  --health-retries 3 \
  --health-start-period 60s \
  --health-timeout 5s \
  registry.gitlab.com/wild-beavers/docker/aws-efs-mount:latest fs-1234567890abcdefg.efs.us-east-1.amazonaws.com
```

For usage outside AWS with custom DNS name:
```shell
sudo podman run -it --rm -d \
  --cap-add=CAP_SYS_ADMIN \
  -v "${HOME}/.aws/credentials":/root/.aws/credentials \
  -v "${HOME}/.aws/config":/root/.aws/config \
  -e "EFS_MOUNT_DNS_SUFFIX=example.com" \
  -e "EFS_MOUNT_STUNNEL_CHECK_CERT_HOSTNAME=false" \
  -v "$(pwd)/mountpoint":/mnt/efs:shared \
  --health-cmd '/usr/local/bin/healthcheck' \
  --health-interval 20s \
  --healthcheck-timeout 5s \
  --health-retries 3 \
  --health-start-period 60s \
  --health-timeout 5s \
  registry.gitlab.com/wild-beavers/docker/aws-efs-mount:latest fs-1234567890abcdefg.efs.us-east-1.example.com
```
## Environment Variables

| Name | Description                                                                                              | Default value |
| --- |----------------------------------------------------------------------------------------------------------| --- |
| `AWS_REGION` | Region of the EFS                                                                                        | `us-east-1` |
| `EFS_MOUNT_DNS_SUFFIX` | Change the DNS suffix of EFS url ([more information here]()) (useful when using EFS outside AWS VPC)     | `amazonaws.com` |
| `EFS_MOUNT_STUNNEL_CHECK_CERT_HOSTNAME` | Enable stunnel certificate hostname (This might be useful if you use custom DNS endpoint)                | `true` |
| `AWS_PROFILE` | AWS [named profile](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-profiles.html) to use | Not defined |
| `EFS_MOUNT_NFS_EXTRA_OPTIONS` | Other option to add to the EFS mount                                                                     | Not defined |
| `NOTIFY_SOCKET` | Path to systemd notify socket where to notify mount is ready                                             | Not defined |

## Notification

This container support [sd_notify(3)](https://www.freedesktop.org/software/systemd/man/sd_notify.html).

When environment variable `NOTIFY_SOCKET` is set, container will send signal `READY=1` to `NOTIFY_SOCKET`

This can be useful when using in a systemd unit.

Here is an example of systemd unit using podman and service type notify:

```bash
[Unit]
Description=Mount EFS
After=network-online.target
After=local-fs.target
After=network-online.target

[Service]
Restart=on-failure
Type=notify
KillMode=none
ExecStartPre=-mkdir -p /mnt/efs
ExecStartPre=-podman stop efs
ExecStartPre=podman pull registry.gitlab.com/wild-beavers/docker/aws-efs-mount:latest
ExecStart=podman run -d --rm --sdnotify=container --cap-add=CAP_SYS_ADMIN --name efs -v "/mnt/efs":/mnt/efs:shared registry.gitlab.com/wild-beavers/docker/aws-efs-mount:latest fs-1234567890abcdefg.efs.us-east-1.example.com
ExecStop=podman stop efs
Environment=NOTIFY_SOCKET=/run/systemd/notify

[Install]
WantedBy=multi-user.target
```

## DNS record resilience

AWS EFS mount points are bound to private AWS domain names.
Moreover, AWS EFS tools limits the URI it accepts to this pattern: `({az}.)?{fs_id}.efs.{region}.{dns_name_suffix}`.
This greatly decrease the resilience of a service with EFS, because the Filesystem ID might be subject to change over time and free CNAMEs are not an option.
To mitigate the issue, you can create a free TXT record containing a CSV with keys:

* `efs-uri`: record that match the EFS util regex. You can still use a custom {dns_name_suffix} here.
* `region`: region of the EFS resource. *This is legacy and will be removed in future major version*.
* `dns-suffix`: suffix of your `efs-uri` DNS record (i.e for `fs-xxxxxxx.efs.us-east-1.example.com`, `dns-suffix` will be `example.com`). *This is legacy and will be removed in future major version*.


By default, the container will try to extract information from the DNS record passed as argument.
You have nothing to do.

For example, from `fs-xxxxxxx.efs.us-east-2.example.com` will be extracted:
* region: `us-east-2`
* dns-suffix: `example.com`

## Limitations

* Authentication methods supported by EFS tools are:
  * Instance security credentials service (IMDS);
  * credentials files;
  * ECS credentials relative URI.

`docker` network driver must be set to `host` because AWS “secure” EFS uses `stunnel` and loopback address as a backend for TLS.
`podman` does not require this driver.

* See https://github.com/aws/efs-utils/issues/110
* `sd_notify` STATUS is not supported (see https://github.com/containers/podman/issues/12636)
