ARG IMAGE="docker.io/library/rust"
ARG IMAGE_TAG="1-slim-bullseye"
ARG EFS_UTILS_VERSION=2.0.1

FROM $IMAGE:$IMAGE_TAG AS builder

ARG EFS_UTILS_VERSION

ARG IMAGE
ARG IMAGE_TAG

ENV EFS_UTILS_VERSION=$EFS_UTILS_VERSION

COPY resources/builder /resources

RUN /resources/build && rm -rf /resources


FROM $IMAGE:$IMAGE_TAG

ARG EFS_UTILS_VERSION

ENV EFS_UTILS_VERSION=$EFS_UTILS_VERSION

ARG IMAGE
ARG IMAGE_TAG
ARG BUILD_DATE
ARG VCS_REF
ARG VCS_DESCRIPTION
ARG VCS_URL
ARG VCS_TITLE
ARG VCS_NAMESPACE
ARG CVS_REF
ARG CVS_DESCRIPTION
ARG CVS_URL
ARG CVS_TITLE
ARG CVS_NAMESPACE
ARG LICENSE
ARG VERSION

VOLUME /data

COPY resources/final /resources
COPY --from=builder /efs-utils-$EFS_UTILS_VERSION/build/*.deb /resources/efs-utils.deb

RUN /resources/build && rm -rf /resources

WORKDIR /data

ENTRYPOINT ["/usr/bin/tini", "-g", "--", "/usr/local/sbin/efs-mount"]

LABEL "org.opencontainers.image.created"=${BUILD_DATE} \
      "org.opencontainers.image.authors"="${VCS_NAMESPACE} - https://gitlab.com/wild-beavers/" \
      "org.opencontainers.image.url"="${VCS_URL}" \
      "org.opencontainers.image.documentation"="${VCS_URL}" \
      "org.opencontainers.image.source"="${VCS_URL}" \
      "org.opencontainers.image.version"="${VERSION}" \
      "org.opencontainers.image.vendor"="${VCS_NAMESPACE}" \
      "org.opencontainers.image.licenses"="${LICENSE}" \
      "org.opencontainers.image.title"="${VCS_TITLE}" \
      "org.opencontainers.image.description"="${VCS_DESCRIPTION}" \
      "org.opencontainers.image.base.name"="${IMAGE}:${IMAGE_TAG}" \

      "org.opencontainers.applications.amazon-efs-utils.version"=$EFS_UTILS_VERSION
