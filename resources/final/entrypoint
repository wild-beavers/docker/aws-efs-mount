#!/usr/bin/env bash

set -e

EFS_MOUNT_CONFIG_FILE_PATH=/etc/amazon/efs/efs-utils.conf

if [[ -n "${DEBUG}" ]]; then
  set -x
  sed -i "s/logging_level =.*/logging_level = DEBUG/g" ${EFS_MOUNT_CONFIG_FILE_PATH}
  sed -i "s/stunnel_debug_enabled =.*/stunnel_debug_enabled = true/g" ${EFS_MOUNT_CONFIG_FILE_PATH}
fi

trap cleanup SIGTERM TERM INT SIGRTMIN+3

function cleanup() {
  echo "Stopping EFS mount docker"
  fusermount -zu /mnt/efs
  exit $?
}

function efs_write_test() {
  local PATH_TEST=$1
  local uuid
  uuid=$(tr -dc A-Za-z0-9 </dev/urandom | head -c 20 ; echo '')
  local end=$((SECONDS+15))
  local result=1

  while [[ ${result} != 0 && ${SECONDS} -lt $end ]]; do
    result=$(touch "${PATH_TEST}"/"${uuid}" > /dev/null 2>&1 && echo 0 || echo 1)
    if [[ ${result} != 0 ]]; then
      sleep 0.5
    fi
  done

  rm "${PATH_TEST}"/"${uuid}" > /dev/null 2>&1 || sleep 0

  echo "${result}"
}

function test_dns() {
  local DNS_NAME=$1
  local end=$((SECONDS+90))
  local result=1

  while [[ ${result} != 0 && ${SECONDS} -lt $end ]]; do
    result=$( (host -t TXT "${DNS_NAME}" > /dev/null 2>&1 && echo 0) || (host -t A "${DNS_NAME}" > /dev/null 2>&1 && echo 0) || echo 1)
    if [[ ${result} != 0 ]]; then
      sleep 3
    fi
  done

  echo "${result}"
}

function efs_mount() {
  local uri="$1"
  local efs_options="$2"
  local retries=${3:-0}

  while [[ $retries -lt 2 ]]; do
      if [[ $(mount -t efs -o tls,iam"$efs_options" "$uri" /mnt/efs && echo 0 || echo 1) != 0 ]]; then
        return 0
      fi

      efs_mount "$uri" "$efs_options" "$((retries+1))"
  done

  >&2 echo "Something went wrong while mounting EFS (mount process exited with $?)"
  exit 2
}


if [ $# -ne 1 ];
  then
    >&2 echo "ERROR: Missing EFS mount point URI"
    exit 1
fi

mkdir -p /mnt/efs

test_dns "$1"

set +e

if ! host -t txt "$1" | grep -qi "descriptive text" > /dev/null 2>&1;
  then
    set -e
else
  set -e
  for raw_record in $(dig txt "${1}" +short); do
    record=$(echo "${raw_record}" | sed -En 's/^"(.*)/\1/p' | sed -En 's/(.*)"$/\1/p')
    key=$(echo "${record}" | cut -d "=" -f 1)
    value=$(echo "${record}" | cut -d "=" -f 2)

    case $key in
      efs-uri)
        echo "‘efs-uri’ key found in TXT record: $value."
        EFS_URI=$value
        ;;
      region)
        echo "‘region’ key found in TXT record: $value. WARNING: this deprecated, you do not need to use TXT anymore, the information is processed from your input."
        AWS_REGION=$value
        ;;
      dns-suffix)
        echo "‘dns-suffix’ key found in TXT record: $value. WARNING: this deprecated, you do not need to use TXT anymore, the information is processed from your input."
        EFS_MOUNT_DNS_SUFFIX=$value
        ;;
      *)
        echo "Unknown key ‘${key}’. Skipping. WARNING: this deprecated, you do not need to use TXT anymore, the information is processed from your input."
        ;;
    esac
  done
fi

DEFAULT_REGION=$(echo "${EFS_URI:-$1}" | grep -oP '(us(-gov)?|ap|ca|cn|eu|sa)-(central|(north|south)?(east|west)?)-\d')
DEFAULT_DNS_SUFFIX=$(echo "${EFS_URI:-$1}" | grep -oP '(?<=-\d\.)(\.?[^\.]+)+$')

REGION=${AWS_REGION:-$DEFAULT_REGION}
DNS_SUFFIX=${EFS_MOUNT_DNS_SUFFIX:-$DEFAULT_DNS_SUFFIX}
CHECK_CERT_HOSTNAME=${EFS_MOUNT_STUNNEL_CHECK_CERT_HOSTNAME:-"true"}
URI=${EFS_URI:-$1}

if [[ -n "${EFS_MOUNT_ENDPOINT_IP}" ]]; then
    echo "${EFS_MOUNT_ENDPOINT_IP} $1" >> /etc/hosts
fi

sed -i "s/.*region =.*/region = ${REGION}/g" ${EFS_MOUNT_CONFIG_FILE_PATH}
sed -i "s/dns_name_suffix =.*/dns_name_suffix = ${DNS_SUFFIX}/g" ${EFS_MOUNT_CONFIG_FILE_PATH}
sed -i "s/.*stunnel_check_cert_hostname =.*/stunnel_check_cert_hostname = ${CHECK_CERT_HOSTNAME}/g" ${EFS_MOUNT_CONFIG_FILE_PATH}

if [[ -n "$AWS_PROFILE" ]]; then
    EFSOPTION=",awsprofile=$AWS_PROFILE"
fi

if [[ -n "$EFS_MOUNT_NFS_EXTRA_OPTIONS" ]]; then
    EFSOPTION="${EFSOPTION},${EFS_MOUNT_NFS_EXTRA_OPTIONS}"
fi

efs_mount "$URI" "$EFSOPTION"

/usr/bin/env amazon-efs-mount-watchdog &

echo "$!" > /tmp/efs_watchdog_pid

write_test_result=$(efs_write_test "/mnt/efs")

if [[ ${write_test_result} != 0 ]]; then
    >&2 echo "EFS was mounted but it seems it remains unreadable"
    exit 3
fi

if [[ -n "$NOTIFY_SOCKET" ]]; then
    echo -n "READY=1" | nc -4u -w1 -U "${NOTIFY_SOCKET}"
fi

echo "EFS is mounted"

sleep infinity &

wait "$!"
